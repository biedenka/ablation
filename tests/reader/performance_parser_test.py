"""
Created on September 15, 2015

@author: Andre Biedenkapp
"""

import unittest
import logging
import os
from ablation.reader.performance_parser import PerformanceParser

class Test(unittest.TestCase):

    def setUp(self):
        self.src_dir = os.path.dirname(os.path.dirname(
                                       os.path.abspath(__file__)))
        self.pp = PerformanceParser(debug=True)

    def test_read_csv_file(self):
        csv_file = os.path.join(self.src_dir, 'files', 'csv',
                                'performance_example.csv')
        inst, suc, cen, perf, conf = self.pp.read_csv_file(csv_file)
        
        self.assertEqual(len(inst), len(suc))
        self.assertEqual(len(perf), len(conf))
        self.assertEqual(len(conf), len(inst))
        self.assertEqual(len(cen), len(perf))
        self.assertEqual(len(suc), 18)
        
        self.assertEqual(len(conf[0]), 1)        
        self.assertEqual(len(conf[1]), 2)
        
    def test_parse_configurations(self):
        csv_file = os.path.join(self.src_dir, 'files', 'csv',
                                'performance_example.csv')
        inst, suc, cen, perf, conf = self.pp.read_csv_file(csv_file)
        configs = self.pp.parse_configurations(conf, {'x1': 2, 'x2': 0.5})
        
        self.assertEqual(len(configs['x1']), len(configs['x2']))
        self.assertEqual(len(configs['x1']), len(perf))
        
        self.assertEqual(configs['x1'][0], 0)
        self.assertEqual(configs['x2'][0], 0.5)        
        self.assertEqual(configs['x1'][len(configs['x1'])-1], 3)
        self.assertEqual(configs['x2'][len(configs['x2'])-1], 0.084)
        
if __name__ == "__main__":
    unittest.main()
