"""
Created on September 22, 2015

@author: Andre Biedenkapp
"""

import unittest
import logging
import os
from ablation.reader.config_space_parser import ConfigSpaceParser
from ablation.reader.performance_parser import PerformanceParser


class Test(unittest.TestCase):
    def setUp(self):
        self.src_dir = os.path.dirname(os.path.dirname(
                                       os.path.abspath(__file__)))
        self.pp = PerformanceParser(debug=True)

    def test_pcs(self):
        pcs_file = os.path.join(self.src_dir, 'files', 'pcs',
                                'test_with_conditionals.pcs')

        pcs = ConfigSpaceParser(pcs_file, debug=True)
        self.assertEqual(len(pcs.parameters), len(pcs.default_dict))
        self.assertEqual(len(pcs.parameters), 6)
        logging.debug('     default: %s' % pcs.default_dict)
        logging.debug('      params: %s' % pcs.parameters)
        logging.debug('conditionals: %s' % pcs.conditional)

    def test_pcs_and_performance_parser_3_params(self):
        pcs_file = os.path.join(self.src_dir, 'files', 'pcs',
                                'test_pcs_3_params.pcs')

        csv_file = os.path.join(self.src_dir, 'files', 'csv',
                                'test_performance_3_params.csv')
        inst, suc, cen, perf, conf = self.pp.read_csv_file(csv_file,
            skip_head=False)

        pcs = ConfigSpaceParser(pcs_file, debug=True)
        configs = self.pp.parse_configurations(conf, pcs.default_dict)

        for i in range(len(configs['a'])):
            strn = ''
            for c in configs:
                strn = strn + str(configs[c][i]) + ', '
            logging.debug('Config %2d: %s, %14s' % (i, configs.keys(),
                strn[:len(strn) - 2]))

        self.assertEqual(isinstance(configs['b'][0], type('somestring')), True)
        self.assertEqual(isinstance(configs['c'][0], type(0.0)), True)
        self.assertEqual(configs['b'][4], '2')
        self.assertEqual(len(configs), 3)
        self.assertEqual(len(configs['b']), 12)

    def test_pcs_and_performance_parser_4_params(self):
        pcs_file = os.path.join(self.src_dir, 'files', 'pcs',
                                'test_pcs_4_params.pcs')

        csv_file = os.path.join(self.src_dir, 'files', 'csv',
                                'test_performance_4_params.csv')
        inst, suc, cen, perf, conf = self.pp.read_csv_file(csv_file,
            skip_head=False)

        pcs = ConfigSpaceParser(pcs_file, debug=True)
        print pcs.default_dict
        configs = self.pp.parse_configurations(conf, pcs.default_dict)

        for i in range(len(configs['a'])):
            strn = ''
            for c in configs:
                strn = strn + str(configs[c][i]) + ', '
            logging.debug('Config %2d: %s, %14s' % (i, configs.keys(),
                strn[:len(strn) - 2]))

        self.assertEqual(isinstance(configs['b'][0], type(1)), True)
        self.assertEqual(isinstance(configs['c'][0], type(0.0)), True)
        self.assertEqual(isinstance(configs['d'][0], type(1)), True)
        self.assertEqual(configs['b'][31], 2)
        self.assertEqual(configs['d'][0], -1)
        self.assertEqual(len(configs), 4)
        self.assertEqual(len(configs['b']), 48)

if __name__ == "__main__":
    unittest.main()
