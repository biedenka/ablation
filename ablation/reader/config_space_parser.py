'''
Created on September 15, 2015

@author: Andre Biedenkapp
'''

import logging
import os
import re

class ConfigSpaceParser(object):
    '''
    Reads the Parameter Configuration Space file
    '''

    def __init__(self, pcs_file, debug=False):
        '''
        Constructor
        '''
        if debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.ERROR)
        self.logger = logging.getLogger('PCS')
        
        self.default_dict = {}
        self.parameters = {}
        self.conditional = []
        self.pcs = self.read_pcs_file(pcs_file)
        
    def read_pcs_file(self, pcs_file, comment_symbol = '#'):
        """
        Reads in a pcs file and extracts the allowed parameter values and of which type
        they are.
        
        :pcs_file: The file to read from
        :comment_symbol: The symbol that denotes comments in the pcs file. If this is not set
                         accordingly this can cause trouble with casting to ints/floats
        """
        num_regex = "[+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?"
        NUMERICAL_REGEX = re.compile("^[ ]*(?P<name>[^ ]+)[ ]*\[[ ]*(?P<range_start>%s)[ ]*,[ ]*(?P<range_end>%s)\][ ]*\[(?P<default>%s)\](?P<misc>.*)$" %(num_regex,num_regex, num_regex))
        CAT_REGEX = re.compile("^[ ]*(?P<name>[^ ]+)[ ]*{(?P<vals>.+)}[ ]*\[(?P<default>[^#]*)\](?P<misc>.*)$")
        COND_REGEX = re.compile("^[ ]*(?P<cond>[^ ]+)[ ]*\|[ ]*(?P<head>[^ ]+)[ ]*in[ ]*{(?P<vals>.+)}(?P<misc>.*)$")
        
        with open(pcs_file) as fp:
            for line in fp:
                clean = line.strip('\n').strip(' ')
                
                if clean == '' or clean[0] == '#': # skip empty or comment lines
                    continue
                    
                # self.logger.debug(clean)
                
                numerical_match = NUMERICAL_REGEX.match(clean)
                cat_match = CAT_REGEX.match(clean)
                cond_match = COND_REGEX.match(clean)
                
                assert ((bool(numerical_match) and bool(cat_match)) and bool(cond_match)) == False, '%s matched as multiple types!' % clean # can only be matched to one type
                    
                if cat_match: # check if we have a categorical ...
                    # ... match and extrac all necessary information, ...
                    name = cat_match.group('name')
                    vals = map(lambda x: x.strip(' '), cat_match.group('vals').split(','))
                    default = cat_match.group('default')
                    type = 2 # of categorical type
                    self.default_dict[name] = default
                    self.parameters[name] = Parameter(name, type, vals, default, '')
                
                if numerical_match: # ... a numerical match ...
                    name = numerical_match.group('name')
                    misc = numerical_match.group('misc').strip(' ')
                    start = numerical_match.group('range_start')
                    end = numerical_match.group('range_end')
                    found = misc.find(comment_symbol)
                    if found > -1: # get rid of comments in misc
                        misc = misc[:found]
                    if 'i' in misc:
                        type = 0
                    else: # of type int (0) or float (1)
                        type = 1
                    if type:
                        default = float(numerical_match.group('default'))
                        vals = [float(start), float(end)]
                    else:
                        default = int(numerical_match.group('default'))
                        vals = [int(start), int(end)]
                    self.default_dict[name] = default
                    self.parameters[name] = Parameter(name, type, vals, default, misc)
                
                if cond_match: # ... or a conditional match
                    head = cond_match.group("head")
                    cond = cond_match.group("cond")
                    vals = map(lambda x : x.strip(" "), cond_match.group("vals").split(","))
                    
                    head_param = self.parameters[head]
                    idxs = []
                    for v in vals:
                        idxs.append(head_param.vals.index(v))
                    
                    self.conditional.append(Condition(head, cond, vals, idxs))
                    
class Condition(object):
    """
    Class to store necessary entries of a condition of the pcs in a neat way
    """
    def __init__(self, head, cond, vals, idxs):
        self.head = head
        self.cond = cond
        self.vals = vals
        self.idxs = idxs
        
    def __repr__(self):
        return "%s | %s in {%s} (%s)" %(self.cond, self.head, ",".join(self.vals), ",".join(map(str,self.idxs)))

class Parameter(object):
    """
    Class to store necessary entries of a parameter of the pcs in a neat way
    """
    def __init__(self, name, type, vals, default, misc):
        self.name = name
        self.type = type
        self.vals = vals
        self.default = default
        
        err_msg = 'Default %s not in %s' % (default, vals)
        
        if self.type == 2:
            assert default in vals, err_msg
        else:
            assert (default >= vals[0] and default <= vals[1]), err_msg
        
        if misc != '':
            pass # TODO handle log (l) and neg-log (L)
            
    def __repr__(self):
        return self.name