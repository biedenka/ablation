## Ablation with Surrogates

This version of Efficient Ablation with surrogates is **not maintained**.
You can find the maintained version as part of the Parameter Importance Package ([PIMP](https://github.com/automl/ParameterImportance)).

### Dependencies
1.   [EPM](https://bitbucket.org/mlindauer/epm) [https://bitbucket.org/mlindauer/epm](https://bitbucket.org/mlindauer/epm)
2.   [Random Forest Run](https://bitbucket.org/aadfreiburg/random_forest_run) [https://bitbucket.org/aadfreiburg/random_forest_run](https://bitbucket.org/aadfreiburg/random_forest_run)

Here are some example calls:

    python scripts/evaluate.py --type epm --model rfr --scenario path_to_aclib_runs/scenario_name/Configurator/run-1/smac-output/aclib --dir ./Test --logy --all path_to_aclib_runs/scenario_name/Configurator
    python scripts/evaluate.py --type epm --model rf --scenario path_to_aclib_runs/scenario_name/Configurator/run-1/smac-output/aclib --dir ./Test --logy