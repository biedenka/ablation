"""
Created on September 16, 2015

@author: Andre Biedenkapp
"""
from __future__ import print_function

import re
import os
import copy
import glob
import json
import argparse
import datetime
import numpy as np

from matplotlib import pyplot as plt
from collections import OrderedDict
from matplotlib import rc

"""
            |          RMSE          |       Matched
-----------------------------------------------------------------
scenario 1  |     surrogate-based Ablation    |   Racing  |    surrogate-based Ablation    |    Racing
scenario 2  |     surrogate-based Ablation    |   Racing  |    surrogate-based Ablation    |    Racing
"""

table_header = '\\begin{tabular}{l|r|r|c|c|r|r}\n' \
               '\\toprule\n' \
                '{}&\\multicolumn{2}{|c|}{RMSE}&\\multicolumn{2}{|c|}{Correct}&\\multicolumn{1}{|c|}{Out-of-bag}&\\multicolumn{1}{|c}{\\#Training}\\\\\n' \
                'Scenario & \\multicolumn{1}{|c|}{surrogate-based Ablation} & \\multicolumn{1}{|c|}{Racing} & \\multicolumn{1}{|c|}{surrogate-based Ablation}' \
               '&\\multicolumn{1}{|c|}{Racing} & \\multicolumn{1}{|c|}{error}&\\multicolumn{1}{|c}{Samples}\\\\\n' \
                '\\midrule\n'
table_row = '%10s & %10.3f & %10.3f & %10d & %10d & %10s & %10s\\\\ %s\n'

time_table_header = '\\begin{tabular}{l|c|c|c|r|r|r|r}\n' \
                    '\\toprule\n' \
                    '{} &\\multicolumn{3}{|c|}{surrogate-based Ablation}&\\multicolumn{2}{|c|}{Racing}&\\multicolumn{2}{|c}{Ground Truth}\\\\\n' \
                    '\\midrule\n' \
                    'Scenario & \\multicolumn{1}{|c|}{Training} &\\multicolumn{1}{|c|}{without model training} & \\multicolumn{1}{|c|}{Validation} & \\multicolumn{1}{|c|}{Training} & \\multicolumn{1}{|c|}{Validation} & \\multicolumn{1}{|c|}{Training} & \\multicolumn{1}{|c}{Validation}\\\\\n' \
                    '\\midrule\n'
time_table_row = '%s & %s & %s & %s & %s & %s & %s & %s\\\\\n'

MAX_SIZE = 19
UP_TIL = 8
FROM = -8


# ----------------------------------------------------------------------------------------------------------------------
def _parse_validation_data(data):
    data = re.sub('  +', ' ', data)
    data = data.replace(', ', ',')
    data = data.split('\n')
    if data[-1] == '':
        data = data[:-1]

    parameters = OrderedDict()
    for line in data[4:-1]:
        line = line.strip().split(' ')[1:5:3]
        if ',' in line[0]:
            line[0] = ','.join(sorted(line[0].split(',')))
        try:
            parameters[line[0]] = float(line[1])
        except IndexError:
            print(line)
    return parameters


# ----------------------------------------------------------------------------------------------------------------------
def _parse_log_file(data):
    data = data.split('\n')
    if data[-1] == '':
        data = data[:-1]

    data = list(map(lambda x: x.strip().split(' '), data))
    timestamps, train_start, train_stop, oob_error, nsamples = _parse_time_stamps(data)
    pre = timestamps[train_start - 1] - timestamps[0]
    post = timestamps[-1] - timestamps[train_stop + 1]

    dict_ = {'timestamps': timestamps, 'train_start_idx': train_start,
             'train_stop_idx': train_stop, 'training_time_no_model_training': pre + post, 'training_time':
             timestamps[-1] - timestamps[0], 'validation_time': None, 'bf_validation_time': None,
             'oob_error': oob_error, 'nsamples': nsamples}

    return dict_


# ----------------------------------------------------------------------------------------------------------------------
def _parse_time_stamps(data):
    stamps = []
    train_start_idx = -1
    train_stop_idx = -1
    oob_error = ''
    n_samples = ''
    for idx, line in enumerate(data):
        if 'Start' in line and train_start_idx == -1:
            train_start_idx = idx
        if 'Done' in line and train_stop_idx == -1:
            train_stop_idx = idx
        if 'error:' in line:
            oob_error = line
        if 'points:' in line:
            n_samples = line[-1]
        try:
            stamps.append(datetime.datetime.strptime(' '.join(line[:2]), '%Y-%m-%d %H:%M:%S,%f'))
        except ValueError:
            print(' '.join(line[:2]))
    return stamps, train_start_idx, train_stop_idx, oob_error, n_samples


# ----------------------------------------------------------------------------------------------------------------------
def _get_all_folders(args):
    folder_name = 'test' if not args.train else 'train'
    epm_result_folder = glob.glob(os.path.join(args.epm_result_folder, '*',
                                               '*par%d-parI%d-%s*imp' % (args.pPAR, args.iPAR, args.model),
                                               'out', folder_name, 'epm_validation*'))
    epm_result_folder += glob.glob(os.path.join(args.epm_result_folder, '*',
                                                '*par%d-parI%d-%s*imp' % (args.pPAR, args.iPAR, args.model),
                                                'out', folder_name, '*tae*'))
    names = []
    for folder in epm_result_folder:
        folder = folder.replace(args.epm_result_folder, '')
        if folder.startswith(os.path.sep):
            folder = folder[1:]
        names.append(folder.split(os.path.sep)[0])

    epm_log_files = glob.glob(os.path.join(args.epm_result_folder, '*',
                                           '*par%d-parI%d-%s*imp' % (args.pPAR, args.iPAR, args.model),
                                           'out', folder_name, 'epm*ablation*.log'))

    bf_result_folder = glob.glob(os.path.join(args.bf_result_folder, folder_name, '*.txt'))
    ordered = []
    ordered_elf = []
    for name in names[::2]:
        for folder in bf_result_folder:
            if name in folder:
                ordered.append(folder)
        for folder in epm_log_files:
            if name in folder:
                ordered_elf.append(folder)

    bf_result_folder = ordered
    epm_log_files = ordered_elf

    del ordered, ordered_elf

    bundled_result = {}
    for name in set(names):
        bundled_result[name] = {}
        for file_ in epm_log_files:
            if name in file_:
                bundled_result[name]['epm_log'] = file_
        for file_ in epm_result_folder:
            if name in file_:
                if file_.endswith('vars.log'):
                    bundled_result[name]['var_file'] = file_
                elif 'tae' in file_:
                    bundled_result[name]['epm_tae'] = file_
                else:
                    bundled_result[name]['epm_file'] = file_
        for file_ in bf_result_folder:
            if name in file_:
                if file_.endswith('-r-valid.txt'):
                    bundled_result[name]['rac_file'] = file_
                else:
                    bundled_result[name]['bf_file'] = file_

    return epm_result_folder, bf_result_folder, epm_log_files, names, bundled_result


# ----------------------------------------------------------------------------------------------------------------------
def percentage(result):
    delta = float(result[0] - result[-1])
    at = 1
    percentages = []
    for i in result[1:-1]:
        percentages.append(((i - result[at-1]) / delta)*100.)
        at += 1
    return percentages


def _handle_data(file_dict, threshold):
    data_dict = {}

    for key in file_dict:
        data_dict[key] = {}
        for sub_key in file_dict[key]:
            with open(file_dict[key][sub_key]) as curr_file:
                if sub_key == 'epm_log':
                    data_dict[key][sub_key] = _parse_log_file(curr_file.read())
                else:
                    data_dict[key][sub_key] = _parse_validation_data(curr_file.read())
                    if sub_key in ['epm_file', 'bf_file', 'rac_file', 'epm_tae']:
                        new_sub_key = 'epm_improvement'
                        if sub_key == 'bf_file':
                            new_sub_key = 'bf_improvement'
                        elif sub_key == 'rac_file':
                            new_sub_key = 'rac_improvement'
                        elif sub_key == 'epm_tae':
                            new_sub_key = 'epm_tae_improvement'
                        data_dict[key][new_sub_key] = percentage(list(data_dict[key][sub_key].values()))

                if sub_key in ['bf_file', 'epm_file', 'rac_file']:
                    head = sub_key.split('_')[0]
                    data_dict[key]['n_%s_most_important' % head] = _get_n_best(data_dict, key, sub_key, threshold)
    return data_dict


def _get_n_best(data_dict, key, sub_key, threshold):
    return list(map(lambda x: x <= threshold,
                data_dict[key][sub_key.replace('file', 'improvement')])).index(False)


def pretty_print_data_dict(data_dict):
    for key in data_dict:
        print(key, ':')
        for sub_key in data_dict[key]:
            try:
                if isinstance(data_dict[key][sub_key], dict):
                    print('\t%20s' % sub_key, ': ', str(list(data_dict[key][sub_key])[:5])[1:-1], ' ... ')
                elif isinstance(data_dict[key][sub_key], list):
                    print('\t%20s' % sub_key, ': ', str(data_dict[key][sub_key][:5])[1:-1], ' ... ')
                else:
                    print('\t%20s' % sub_key, ': ', data_dict[key][sub_key])
            except TypeError:
                print('\t%20s' % sub_key, ': ', data_dict[key][sub_key])
        print('\t Time taken to train : %s' % data_dict[key]['epm_log']['training_time'])
        print('\t%20s : %s' % ('OOB-Error', data_dict[key]['epm_log']['oob_error']))
        print()


def negative_float(value):
    try:
        value = float(value)
        if value < 0.:
            return float(value)
        if value >= 0.:
            return -1. * float(value)
    except Exception as e:
        print(e)
        raise argparse.ArgumentTypeError("Unable to cast %s to negative float!" % value)


def shorten_parameter_names(path):
    for idx, elem in enumerate(path):
        if ',' in elem:
            elem = elem.split(',')
            if len(elem[0]) > 5:
                elem[0] = elem[0][:5] + '...'
            if len(elem[1]) > 5:
                elem[1] = elem[1][:5] + '...'
            path[idx] = ', '.join(elem)
        elif len(elem) > MAX_SIZE:
            path[idx] = elem[:UP_TIL] + '...' + elem[FROM:]
    return path


def get_min_max(brute_force_performance, epm_performance, upper, lower):
    max_ = max(max(max(brute_force_performance), max(epm_performance)), max(upper))
    min_ = min(min(min(brute_force_performance), min(epm_performance)), min(lower))
    if min_ <= 0:
        min_ += 0.1 * min_
    else:
        min_ -= 0.1 * min_
    if max_ <= 0:
        max_ -= 0.1 * max_
    else:
        max_ += 0.1 * max_
    return min_, max_


def one_plot(brute_force_path, brute_force_performance, epm_path, epm_performance, epm_variance=None,
             x_axis_lower_label='Original', x_axis_upper_label='surrogate-based Ablation', plot_name=None, std=True,
             font=None, lw=6,
             fontsize=38, third_curve=None):
    if font is not None:
        rc('font', **font)

    epm_color = (0.45, 0.45, 0.45)
    brute_force_color = (0, 0, 0)

    if std and epm_variance is not None:
        upper = list(map(lambda x, y: x + np.sqrt(y), epm_performance, epm_variance))
        lower = list(map(lambda x, y: x - np.sqrt(y), epm_performance, epm_variance))
    elif epm_variance is not None:
        upper = list(map(lambda x, y: x + y, epm_performance, epm_variance))
        lower = list(map(lambda x, y: x - y, epm_performance, epm_variance))
    else:
        upper = np.zeros(len(brute_force_path))
        lower = np.zeros(len(brute_force_path))

    epm_path = shorten_parameter_names(epm_path)
    brute_force_path = shorten_parameter_names(brute_force_path)

    fig = plt.figure(figsize=(14, 18))
    ax1 = fig.add_subplot(111)
    ax2 = ax1.twiny()
    plt.subplots_adjust(bottom=0.25, top=0.75, left=0.05, right=.95)

    if epm_variance is not None:
        ax1.fill_between(list(range(len(brute_force_performance))), upper, lower,
                         alpha=0.2, color=brute_force_color, zorder=77)
    ax1.plot(list(range(len(epm_performance))), epm_performance,
             label=x_axis_upper_label, color=brute_force_color, ls='--', lw=lw, zorder=80)
    if third_curve is not None:
        ax1.plot(list(range(len(epm_performance))), third_curve,
                 label='surrogate real target runs', color=brute_force_color, dashes=[1, 10, 1, 20, 20, 20]*100,
                  aa=True, dash_capstyle='round', dash_joinstyle='round', lw=lw, zorder=79)
    ax1.plot(list(range(len(brute_force_performance))), brute_force_performance,
             label=x_axis_lower_label, color=epm_color, lw=lw, zorder=78)

    ax1.set_xlabel(x_axis_lower_label + ' path', color=epm_color, fontsize=fontsize)
    ax1.set_xticks(list(range(len(brute_force_path))))
    ax2.set_xlabel(x_axis_upper_label + ' path', color=(0, 0, 0), fontsize=fontsize)
    ax2.set_xticks(list(range(len(epm_path))))

    ax1.set_xticklabels(brute_force_path, rotation=25, ha='right', color=epm_color)
    ax2.set_xticklabels(epm_path, rotation=-25, ha='right', color=brute_force_color)

    ax1.set_xlim(0, len(brute_force_path) - 1)
    ax2.set_xlim(0, len(epm_path) - 1)

    ax1.legend()
    ax1.set_ylabel('runtime [sec]', fontsize=fontsize, zorder=81)
    ax1.xaxis.grid(True)
    gl = ax1.get_xgridlines()
    for l in gl:
        l.set_linewidth(5)
    if upper[0] == 0 and lower[0] == 0:
        min_ = min(min(brute_force_performance), min(epm_performance))
        max_ = max(max(brute_force_performance), max(epm_performance))
    else:
        min_, max_ = get_min_max(brute_force_performance, epm_performance, upper, lower)
    if int(max_) % 10 == 0:
        max_ -= 1
    ax1.set_ylim(min_, max_)
    handles, labels = ax1.get_legend_handles_labels()

    # reverse the order
    ax1.legend(handles[::-1], labels[::-1])
    plt.tight_layout()

    print('\tPlotting: %s' % plot_name)

    if plot_name is not None:
        plt.savefig(plot_name)


def rmse_penalized_wrong_paired_flips(ground_truth_parameters, comparison_path_parameters,
                                      ground_truth_improvement, comparison_path_improvement, comparison_name):
    squared_diffs = []
    epm_keys_copy = copy.deepcopy(comparison_path_parameters)
    for bf_idx, key_ in enumerate(ground_truth_parameters):
        if key_ in epm_keys_copy:
            idx = comparison_path_parameters.index(key_)
            squared_diffs.append(np.power(comparison_path_improvement[idx] - ground_truth_improvement[bf_idx], 2))
            epm_keys_copy.pop(epm_keys_copy.index(key_))
        else:
            if ',' in key_:
                parent_parameter, child_parameter = key_.split(',')
                if parent_parameter in epm_keys_copy and child_parameter in epm_keys_copy:
                    a_idx = comparison_path_parameters.index(parent_parameter)
                    b_idx = comparison_path_parameters.index(child_parameter)
                    epm_combined_imp = comparison_path_improvement[a_idx] + comparison_path_improvement[b_idx]
                    squared_diffs.append(np.power(epm_combined_imp - ground_truth_improvement[bf_idx], 2))
                    epm_keys_copy.pop(epm_keys_copy.index(parent_parameter))
                    epm_keys_copy.pop(epm_keys_copy.index(child_parameter))
                elif parent_parameter in comparison_path_parameters:
                    a_idx = comparison_path_parameters.index(parent_parameter)
                    squared_diffs.append(np.power(
                        comparison_path_improvement[a_idx] - ground_truth_improvement[bf_idx], 2))
                    epm_keys_copy.pop(epm_keys_copy.index(parent_parameter))
                elif child_parameter in comparison_path_parameters:
                    a_idx = comparison_path_parameters.index(child_parameter)
                    squared_diffs.append(np.power(
                        comparison_path_improvement[a_idx] - ground_truth_improvement[bf_idx], 2))
                    epm_keys_copy.pop(epm_keys_copy.index(child_parameter))
                else:
                    print(key_, 'not in %s_path' % comparison_name)
                    squared_diffs.append(np.power(0 - ground_truth_improvement[bf_idx], 2))
            else:
                print(key_, 'not in %s_path' % comparison_name)
                squared_diffs.append(np.power(0 - ground_truth_improvement[bf_idx], 2))

    for key_ in epm_keys_copy:
        idx = comparison_path_parameters.index(key_)
        if key_ in ground_truth_parameters:
            bf_idx = ground_truth_parameters.index(key_)
            squared_diffs.append(np.power(comparison_path_improvement[idx] - ground_truth_improvement[bf_idx], 2))
        else:
            if ',' in key_:
                parent_parameter, child_parameter = key_.split(',')
                if parent_parameter in ground_truth_parameters and child_parameter in ground_truth_parameters:
                    a_idx = ground_truth_parameters.index(parent_parameter)
                    b_idx = ground_truth_parameters.index(child_parameter)

                    gt_combined_imp = ground_truth_improvement[a_idx] + ground_truth_improvement[b_idx]
                    squared_diffs.append(np.power(comparison_path_improvement[idx] - gt_combined_imp, 2))

                elif parent_parameter in ground_truth_parameters:
                    a_idx = ground_truth_improvement.index(parent_parameter)
                    squared_diffs.append(np.power(
                        comparison_path_improvement[idx] - ground_truth_improvement[a_idx], 2))

                elif child_parameter in ground_truth_parameters:
                    a_idx = ground_truth_parameters.index(child_parameter)
                    squared_diffs.append(np.power(
                        comparison_path_improvement[idx] - ground_truth_improvement[a_idx], 2))

                else:
                    print(key_, 'not in %s_path' % 'ground_truth')
                    squared_diffs.append(np.power(0 - ground_truth_improvement[bf_idx], 2))
            else:
                print(key_, 'not in %s_path' % 'ground_truth')
                squared_diffs.append(np.power(0 - ground_truth_improvement[bf_idx], 2))

    return np.sqrt(np.mean(squared_diffs))


def speedup(n, values):
    return values[0] / values[n]


def process_results(data_dict, outdir, plot_path_len=10, plot_font=None):
    table = table_header
    time_table = time_table_header
    plot_path_len += 1
    for key in data_dict:
        folder = os.path.join(outdir, key)
        if not os.path.exists(folder):
            os.mkdir(folder)

        max_len = min(min(len(data_dict[key]['epm_file']),
                          len(data_dict[key]['bf_file'])),
                      len(data_dict[key]['rac_file']))
        if plot_path_len > max_len:
            plot_path_len = max_len


        one_plot(list(data_dict[key]['bf_file'].keys())[:plot_path_len],
                 list(data_dict[key]['bf_file'].values())[:plot_path_len],
                 list(data_dict[key]['epm_file'].keys())[:plot_path_len],
                 list(data_dict[key]['epm_file'].values())[:plot_path_len],
                 list(data_dict[key]['var_file'].values())[:plot_path_len],
                 x_axis_lower_label='ground truth', x_axis_upper_label='surrogate-based ablation',
                 plot_name=os.path.join(folder, key + '-no-tae.png'), font=plot_font)
        try:
            one_plot(list(data_dict[key]['bf_file'].keys())[:plot_path_len],
                     list(data_dict[key]['bf_file'].values())[:plot_path_len],
                     list(data_dict[key]['epm_file'].keys())[:plot_path_len],
                     list(data_dict[key]['epm_file'].values())[:plot_path_len],
                     list(data_dict[key]['var_file'].values())[:plot_path_len],
                     x_axis_lower_label='ground truth', x_axis_upper_label='surrogate-based ablation',
                     plot_name=os.path.join(folder, key + '.png'), font=plot_font,
                     third_curve=list(data_dict[key]['epm_tae'].values())[:plot_path_len])
        except KeyError:
            pass

        one_plot(list(data_dict[key]['bf_file'].keys())[:plot_path_len],
                 list(data_dict[key]['bf_file'].values())[:plot_path_len],
                 list(data_dict[key]['rac_file'].keys())[:plot_path_len],
                 list(data_dict[key]['rac_file'].values())[:plot_path_len],
                 None, x_axis_lower_label='ground truth', x_axis_upper_label='racing',
                 plot_name=os.path.join(folder, key + '-race.png'), font=plot_font)

        epm_matching, racing_matching = 0, 0

        n_bf = data_dict[key]['n_bf_most_important']
        n_epm = data_dict[key]['n_epm_most_important']
        n_rac = data_dict[key]['n_rac_most_important']
        rac_keys = list(data_dict[key]['rac_file'].keys())[1:-1]
        epm_keys = list(data_dict[key]['epm_file'].keys())[1:-1]
        bf_keys = list(data_dict[key]['bf_file'].keys())[1:-1]

        bf_matching_epm, bf_matching_rac = 0, 0

        for param in epm_keys[:n_epm]:
            if param in bf_keys[:n_epm]:
                bf_matching_epm += 1

        for param in rac_keys[:n_rac]:
            if param in bf_keys[:n_rac]:
                bf_matching_rac += 1

        for param in bf_keys[:n_bf]:
            if param in epm_keys[:n_bf]:
                epm_matching += 1
            if param in rac_keys[:n_bf]:
                racing_matching += 1

        # print('EPM-BF: %d/%d' % (bf_matching_epm, n_epm))
        # print('BF-EPM: %d/%d' % (epm_matching, n_bf))
        tmp = ''
        tmp += ' %3s | %3s | %3s | %3s \n' % ('P/T', 'BF', 'EPM', 'ACE')
        tmp += '-----------------------\n'
        tmp += ' %3s | %1d/%1d | %1d/%1d | %1d/%1d \n' % ('BF', n_bf, n_bf, bf_matching_epm, n_epm,
                                                        bf_matching_rac, n_rac)
        tmp += ' %3s | %1d/%1d | %1d/%1d | %1d/%1d \n' % ('EPM', epm_matching, n_bf, n_epm, n_epm, 0, n_rac)
        tmp += ' %3s | %1d/%1d | %1d/%1d | %1d/%1d \n' % ('RAC', racing_matching, n_bf, 0, n_epm, n_rac, n_rac)
        with open(os.path.join(outdir, key + '_conf.txt'), 'w') as f:
            f. write(tmp)
        # print('RAC-BF: %d/%d' % (bf_matching_epm, n_rac))
        # print('BF-RAC: %d/%d' % (racing_matching, n_bf))

        epm_improvement = data_dict[key]['epm_improvement']
        rac_improvement = data_dict[key]['rac_improvement']
        bf_improvement = data_dict[key]['bf_improvement']
        try:
            epm_tae_improvement = data_dict[key]['epm_tae_improvement']
        except:
            epm_tae_improvement = epm_improvement

        epm_rmse = rmse_penalized_wrong_paired_flips(bf_keys, epm_keys,
                                                     bf_improvement, epm_improvement, 'epm')
        rac_rmse = rmse_penalized_wrong_paired_flips(bf_keys, rac_keys,
                                                     bf_improvement, rac_improvement, 'racing')

        with open(os.path.join(folder, 'bf-epm_tae-rmse.txt'), 'w') as bfepmtae_file:
            bfepmtae_file.write('RMSE between BF & EPM-TAE: %s\n' % str(rmse_penalized_wrong_paired_flips(
                bf_keys, epm_keys, bf_improvement, epm_tae_improvement, 'epm-tae')))
            bfepmtae_file.write('Speedup full ablation on the %d most important parameters: %2.3f\n' % (n_bf,
                                speedup(n_bf, list(data_dict[key]['bf_file'].values()))))
            try:
                bfepmtae_file.write('Speedup surro ablation on the first %d parameters: %2.3f\n' % (n_bf,
                                    speedup(n_bf, list(data_dict[key]['epm_tae'].values()))))
            except KeyError:
                bfepmtae_file.write('No epm tae\n')
            bfepmtae_file.write('Speedup racing ablation on the first %d parameters: %2.3f\n' % (n_bf,
                                speedup(n_bf, list(data_dict[key]['rac_file'].values()))))
            len_ = len(list(data_dict[key]['bf_file'].values())) - 1
            bfepmtae_file.write('Speedup between source and target: %2.3f\n' %
                                speedup(len_, list(data_dict[key]['bf_file'].values())))

        print('\n%s' % key)
        print('EPM-EPM_TAE RMSE:', rmse_penalized_wrong_paired_flips(epm_keys, epm_keys, epm_tae_improvement,
                                                                     epm_improvement, 'epm-tae'))
        print(' BF-EPM_TAE RMSE:', rmse_penalized_wrong_paired_flips(bf_keys, epm_keys,
                                                                     bf_improvement, epm_tae_improvement, 'epm-tae'))

        table += table_row % (key, epm_rmse, rac_rmse, epm_matching, racing_matching,
                              data_dict[key]['epm_log']['oob_error'],
                              data_dict[key]['epm_log']['nsamples'],
                              '% of ' + str(n_bf) + ' important brute-force parameters')
        time_table += time_table_row % (key, str(data_dict[key]['epm_log']['training_time'])[:-7],
                                        str(data_dict[key]['epm_log']['training_time_no_model_training'])[:-7],
                                        str(None), str(None), str(None), str(None), str(None))


    table += '\\bottomrule\n\\end{tabular}\n'
    time_table += '\\bottomrule\n\\end{tabular}\n'

    print('\nGenerating result table:')
    with open(os.path.join(outdir, 'scenario_table.tex'), 'w') as tex_file:
        tex_file.write(table)
    print('\nGenerating time table:')
    with open(os.path.join(outdir, 'time_table.tex'), 'w') as tex_file:
        tex_file.write(time_table)

    print()
    print(table)
    print('Table was written to', os.path.join(outdir, 'scenario_table.tex'))
    print()
    print(time_table)
    print('Table was written to', os.path.join(outdir, 'time_table.tex'))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('epm_result_folder', help='Path to the epm result folder.')
    parser.add_argument('bf_result_folder', help='Path to the brute force validation file.')
    parser.add_argument('--plotFont', default=None, help='Path to a json file specifying the font for the plots.')
    parser.add_argument('--pPAR', default=10, type=int, help='PAR value during prediction.', choices=[1, 10])
    parser.add_argument('--iPAR', default=1, type=int, help='PAR value during imputation.', choices=[1, 10])
    parser.add_argument('--model', default='rfrm', help='Hyperparameter settings of the model.',
                        choices=['rfr', 'rfrm'])
    parser.add_argument('--imprThreshold', default=-5., type=negative_float, help='Improvement Threshold \
                        to determine the n most important parameters', dest='thresh')
    parser.add_argument('--outDir', default='./ablation_results', help='Where to generate the results.')
    parser.add_argument('--plotPathLen', default=5, help='Path length in plots')
    parser.add_argument('--train', action='store_true', help='Use the data generated on TRAIN instead of TEST')
    args, unknown = parser.parse_known_args()
    return args


# ----------------------------------------------------------------------------------------------------------------------
def main():
    args = parse_args()

    if not os.path.exists(args.outDir):
        os.mkdir(args.outDir)

    epm_result_folder, bf_result_folder, epm_log_files, names, file_dict = _get_all_folders(args)

    print('Looking for all necessary files:')
    for key in file_dict:
        print(key, ':')
        for sub_key in file_dict[key]:
            print('\t%20s' % sub_key, ': ', file_dict[key][sub_key])
        print()
    # assert len(epm_result_folder) == len(bf_result_folder)

    if args.plotFont is not None and os.path.exists(args.plotFont):
        with open(args.plotFont, 'r') as font_file:
            font = json.load(font_file)
    else:
        font = None

    print()
    print('#'*120)
    print('Parsing Data')
    data_dict = _handle_data(file_dict, args.thresh)
    pretty_print_data_dict(data_dict)
    print('Parsed Data')
    print('#'*120)
    print('Processing Data')
    process_results(data_dict, args.outDir, args.plotPathLen, plot_font=font)


if __name__ == '__main__':
    main()