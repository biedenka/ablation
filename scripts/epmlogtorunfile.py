import os
import re
import argparse
import logging


def configDictToString(dict_):
    result = ''
    for i in dict_:
        result += '-'+i+" '"+dict_[i]+"' "
    return ' '.join(['Configuration', ':', ''.join(['"', result.strip(), '"'])])


def main():
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    parser = argparse.ArgumentParser()
    parser.add_argument('epmlog')
    args, unknown = parser.parse_known_args()

    file_path = os.path.abspath(args.epmlog)

    rounds, path, source, target = readData(file_path)
    parseData(rounds, path, source, target, os.path.dirname(file_path))


def readData(file_path):
    with open(file_path, 'r') as f:
        data = f.read()

    data = re.sub('  +', ' ', data)
    data = re.sub(', ', ',', data)
    data = data.strip().strip('\n').split('\n')
    rounds = list()
    read_params = False
    source, target, = {}, {}
    path = list()
    for line in data:
        if ', ' in line:
            print(line)
        line = line.strip().split(' ')[3:]
        if read_params:
            if line[0] in ['Predicted']:
                read_params = False
            else:
                param = line[0].strip(':')
                source[param] = line[1]
                target[param] = line[3]
        if line[0] in ['Parameter', 'parameter', 'parameter:', 'Parameter:']:
            read_params = True
        if line[0] not in ['round', 'Round']:
            continue
        if line[3].startswith('['):
            split = line[3].split(',')
            line[3] = ', '.join((split[0][2:-1], split[1][1:-2]))
            split = line[4].split(',')
            line[4] = ', '.join((split[0][2:-1], split[1][1:-2]))
            split = line[7].split(',')
            line[7] = ', '.join((split[0][2:-1], split[1][1:-2]))
        print(line[0], line[1], line[3], line[4], line[7])
        rounds.append(' '.join([' '.join([line[0], line[1]]) + ':', 'Flipped', line[3], 'from',
                      line[4], 'to', line[7]]))
        path.append(line[3])

    return rounds, path, source, target


def parseData(rounds, path, sourcedict, targetdict, save_path):
    """
    <zero or more lines, they are discarded>
    ^Total number of rounds: ([0-9]*)$
    <one line that is discarded (specifics in the tool output)>
    <Two lines for each ablation round (the number in the line above)>

    A regular expression for the first ablation round line is "^Round ([0-9]*): Flipped (.*) from (.*) to (.*)$".
    A regular expression for the second ablation round line is "^Configuration : \"(.*)\"$"
    """
    contents = ['\n'.join(['Total number of rounds: %d' % len(rounds), 'Discard this line'])]
    for idx, round_ in enumerate(rounds):
        if ', ' in path[idx]:
            tmp = path[idx].split(', ')
            sourcedict[tmp[0]] = targetdict[tmp[0]]
            sourcedict[tmp[1]] = targetdict[tmp[1]]
        else:
            sourcedict[path[idx]] = targetdict[path[idx]]
        contents.append('\n'.join([round_, configDictToString(sourcedict)]))

    with open(os.path.join(save_path, 'ablationPath.txt'), 'w') as ablationFile:
        ablationFile.write('\n'.join(contents))


if __name__ == '__main__':
    main()
