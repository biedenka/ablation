from __future__ import print_function

import os
import re
import glob
import argparse

from collections import OrderedDict


# ----------------------------------------------------------------------------------------------------------------------
def _get_all_folders(args):
    epm_result_folder = glob.glob(os.path.join(args.epm_result_folder, '*',
                                               '*par%d-parI%d-%s*imp' % (args.pPAR, args.iPAR, args.model),
                                               'out', 'epm_validation*'))
    epm_result_folder += glob.glob(os.path.join(args.epm_result_folder, '*',
                                               '*par%d-parI%d-%s*imp' % (args.pPAR, args.iPAR, args.model),
                                               'out', '*tae*'))
    names = []
    for folder in epm_result_folder:
        folder = folder.replace(args.epm_result_folder, '')
        if folder.startswith(os.path.sep):
            folder = folder[1:]
        names.append(folder.split(os.path.sep)[0])

    epm_log_files = glob.glob(os.path.join(args.epm_result_folder, '*',
                                               '*par%d-parI%d-%s*imp' % (args.pPAR, args.iPAR, args.model),
                                               'out', 'epm*ablation*'))

    bf_result_folder = glob.glob(os.path.join(args.bf_result_folder, '*.txt'))
    ordered = []
    ordered_elf = []
    for name in names[::2]:
        for folder in bf_result_folder:
            if name in folder:
                ordered.append(folder)
        for folder in epm_log_files:
            if name in folder:
                ordered_elf.append(folder)

    bf_result_folder = ordered
    epm_log_files = ordered_elf

    del ordered, ordered_elf

    bundled_result = OrderedDict()
    for name in set(names):
        bundled_result[name] = OrderedDict()
        for file_ in epm_log_files:
            if name in file_:
                bundled_result[name]['epm_log'] = file_
        for file_ in epm_result_folder:
            if name in file_:
                if file_.endswith('vars.log'):
                    bundled_result[name]['var_file'] = file_
                elif 'tae' in file_:
                    bundled_result[name]['epm_tae'] = file_
                else:
                    bundled_result[name]['epm_file'] = file_
        for file_ in bf_result_folder:
            if name in file_:
                if file_.endswith('-r-valid.txt'):
                    bundled_result[name]['rac_file'] = file_
                else:
                    bundled_result[name]['bf_file'] = file_

    return epm_result_folder, bf_result_folder, epm_log_files, names, bundled_result


# ----------------------------------------------------------------------------------------------------------------------
def config_dict_to_string(dict_, order):
    return ', '.join(map(lambda x: x + '=' + "'" + str(dict_[x]) + "'", order))


def readData(file_path):
    with open(file_path, 'r') as f:
        data = f.read()

    data = re.sub('  +', ' ', data)
    data = re.sub(', ', ',', data)
    data = data.strip().strip('\n').split('\n')
    rounds = list()
    read_params = False
    source, target, = OrderedDict(), OrderedDict()
    path = list()
    for line in data:
        if ', ' in line:
            print(line)
        line = line.strip().split(' ')[3:]
        if read_params:
            if line[0] in ['Predicted']:
                read_params = False
            else:
                param = line[0].strip(':')
                source[param] = line[1]
                target[param] = line[3]
        if line[0] in ['Parameter', 'parameter', 'parameter:', 'Parameter:']:
            read_params = True
        if line[0] not in ['round', 'Round']:
            continue
        if line[3].startswith('['):
            split = line[3].split(',')
            line[3] = ', '.join((split[0][2:-1], split[1][1:-2]))
            split = line[4].split(',')
            line[4] = ', '.join((split[0][2:-1], split[1][1:-2]))
            split = line[7].split(',')
            line[7] = ', '.join((split[0][2:-1], split[1][1:-2]))
        rounds.append(' '.join([' '.join([line[0], line[1]]) + ':', 'Flipped', line[3], 'from',
                      line[4], 'to', line[7]]))
        path.append(line[3])

    return rounds, path, source, target


# ----------------------------------------------------------------------------------------------------------------------
def config_string_to_dict(config):
    dict_ = OrderedDict()
    config = config.split(', ')
    for param_assignment in config:
        param, assignment = param_assignment.replace('"', '').replace("'", '').split('=')
        dict_[param] = assignment
    return dict_


# ----------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('epm_result_folder', help='Path to the epm result folder.')
    parser.add_argument('merged_folder')
    parser.add_argument('--pPAR', default=10, type=int, help='PAR value during prediction.', choices=[1, 10])
    parser.add_argument('--iPAR', default=1, type=int, help='PAR value during imputation.', choices=[1, 10])
    parser.add_argument('--model', default='rfrm', help='Hyperparameter settings of the model.',
                        choices=['rfr', 'rfrm'])
    args_, unknown = parser.parse_known_args()
    args_.__dict__['bf_result_folder'] = 'nonexistingpath'

    erf, brf, elf, n, files = _get_all_folders(args_)

    print('Relevant files:')
    for name in set(n):
        # print(name, args_.merged_folder, name in args_.merged_folder.lower())
        if name in args_.merged_folder.lower():
            print('%20s:\t' % name, files[name]['epm_log'])
            merged_files = glob.glob(os.path.join(args_.merged_folder, '*-it*'))
            if merged_files[0].endswith('.txt'):
                params_string_file = merged_files[0]
                rnr = merged_files[1]
            else:
                params_string_file = merged_files[1]
                rnr = merged_files[0]

            with open(params_string_file, 'r') as psf:
                data = psf.read().split('\n')

            with open(rnr, 'r') as rnr_f:
                rnr_data = rnr_f.read().split('\n')

            if rnr_data[-1] == '':
                rnr_data = rnr_data[:-1]
            rnr_data = list(map(lambda x: x.split(',')[1], rnr_data))

            if data[-1] == '':
                data = data[:-1]

            ids = list(map(lambda x: x.split(': ')[0], data))
            data = list(map(lambda x: x.split(': ')[1], data))

            # order = list(map(lambda x: x.split(', '), data[0]))
            order = list(map(lambda x: x.replace('"', '').replace("'", "").split('=')[0], data[0].split(', ')))

            rounds, path, s, t = readData(files[name]['epm_log'])
            files[name]['source'] = config_dict_to_string(s, order)
            files[name]['target'] = config_dict_to_string(t, order)
            for idx, round_ in enumerate(rounds):
                if ', ' in path[idx]:
                    tmp = path[idx].split(', ')
                    s[tmp[0]] = t[tmp[0]]
                    s[tmp[1]] = t[tmp[1]]
                else:
                    s[path[idx]] = t[path[idx]]
                r_string = config_dict_to_string(s, order)
                if not r_string == files[name]['target']:
                    files[name][idx] = r_string

            # counts = {'source': len(list(filter(lambda x: x == ids[data.index(files[name]['source'])], rnr_data))),
            #           'target': len(list(filter(lambda x: x == ids[data.index(files[name]['target'])], rnr_data)))}
            # # print(counts)
            # for idx in range(len(rounds)):
            #     try:
            #         counts[idx] = len(list(filter(lambda x: x == ids[data.index(files[name][idx])], rnr_data)))
            #     except ValueError:
            #         counts[idx] = 0
            # print('Out of %d runs we observed x, y times:' % len(rnr_data))
            # print(counts)

            param_counts = {}
            for key in s:
                if s[key] == t[key]:
                    continue
                if key not in param_counts:
                    param_counts[key] = [0, 0]
                print(key, s[key], t[key])
                for date in data:
                    date = config_string_to_dict(date)
                    if date[key] == s[key]:
                        param_counts[key][0] += 1
                    if date[key] == t[key]:
                        param_counts[key][1] += 1
            print('Out of %d configurations the parameters were [x, y] times set to the [source, target] value:' %
                  len(data))
            for key in param_counts:
                print(key, ':', param_counts[key])
