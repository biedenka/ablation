import re
import os
import logging
import argparse
import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics import mean_squared_error as mse

from compare_validation import *


# ---------------------------------------------------------------------------------------------------------------------
def main():
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    parser = argparse.ArgumentParser()
    parser.add_argument('validFile_one')  # org
    parser.add_argument('validFile_two')  # epm
    parser.add_argument('validFile_three')  # race
    args, unknown = parser.parse_known_args()

    validation_one = readData(args.validFile_one)
    validation_two = readData(args.validFile_two)
    validation_three = readData(args.validFile_three)

    var_one_dict, var_one_list, source_val_one, target_val_one, result_one = parseContents(validation_one)  # original

    var_two_dict, var_two_list, source_val_two, target_val_two, result_two = parseContents(validation_two)  # epm

    var_three_dict, var_three_list, source_val_three, target_val_three, result_three = parseContents(validation_three)

    # find out if and which parameter is not in both paths
    # This indicates a deacitvated parameter with 0 importance
    not_in_one, not_in_two = diff_param_list(var_one_list, var_two_list)
    
    if not_in_one != []:
        var_one_list = var_one_list[:-1]
        tmp = result_one[-1]
        result_one = result_one[:-1]
        dict_tmp = var_one_dict['-target-']
        for i in not_in_one:
            var_one_list.append(i)
            result_one.append(result_one[-1])
            var_one_dict[i] = dict_tmp
            dict_tmp += 1
        var_one_dict['-target-'] = dict_tmp
        result_one.append(tmp)
        var_one_list.append('-target-')
        
    
    if not_in_two != []:
        var_two_list = var_two_list[:-1]
        tmp = result_two[-1]
        result_two = result_two[:-1]
        dict_tmp = var_two_dict['-target-']
        for i in not_in_two:
            var_two_list.append(i)
            result_two.append(result_two[-1])
            var_two_dict[i] = dict_tmp
            dict_tmp += 1
        var_two_dict['-target-'] = dict_tmp
        result_two.append(tmp)
        var_two_list.append('-target-')
        
    
    not_in_one, not_in_three = diff_param_list(var_one_list, var_three_list)
    
    if not_in_one != []:
        var_one_list = var_one_list[:-1]
        tmp = result_one[-1]
        result_one = result_one[:-1]
        dict_tmp = var_one_dict['-target-']
        for i in not_in_one:
            var_one_list.append(i)
            result_one.append(result_one[-1])
            var_one_dict[i] = dict_tmp
            dict_tmp += 1
        var_one_dict['-target-'] = dict_tmp
        result_one.append(tmp)
        var_one_list.append('-target-')
        
    
    if not_in_three != []:
        var_three_list = var_three_list[:-1]
        tmp = result_three[-1]
        result_three = result_three[:-1]
        dict_tmp = var_three_dict['-target-']
        for i in not_in_three:
            var_three_list.append(i)
            result_three.append(result_three[-1])
            var_three_dict[i] = dict_tmp
            dict_tmp += 1
        var_three_dict['-target-'] = dict_tmp
        result_three.append(tmp)
        var_three_list.append('-target-')

    per_two = percentage(result_two)
    per_one = percentage(result_one)
    per_three = percentage(result_three)

    two_order = []
    three_order = []
    for i in var_one_list:
        if i.startswith('-'):
            continue
        two_order.append(var_two_dict[i])
        three_order.append(var_three_dict[i])

    rmse_a = np.sqrt(mse(per_one, np.array(per_two)[two_order]))
    rmse_b = np.sqrt(mse(per_one, np.array(per_three)[three_order]))

    tmp_a = 0
    tmp_b = 0
    print '\\begin{tabular}{l rrr}\n\\toprule\nparameter & Original & EPM & Racing\\\\\n\\midrule'
    is_true = True
    for i, j, k, l in zip(var_one_list[1:-1], per_one, np.array(per_two)[two_order], np.array(per_three)[three_order]):
        if not is_true:
            print '\\hline'
        print '%s & %03.2f & %03.2f & %03.2f\\\\' % (i, j, k, l)
        is_true = False
    print '\\bottomrule'
    print '\\end{tabular}'

    print
    print '\nEPM: %3.5f\tRacing: %3.5f' % (rmse_a, rmse_b)
    print sum(per_one), sum(per_two), sum(per_three)


if __name__ == '__main__':
    main()
